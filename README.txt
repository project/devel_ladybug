# Devel Ladybug

Thank you for downloading Devel Ladybug. This module provides the Ladybug PHP Dumper for use with Devel. 

## Installation

The recommended method for installation is via Composer. However, you can still download this module to the contrib folder if you already have both Devel and Ladybug installed.

## Configuration

Just go to admin/config/development/devel and select Ladybug as your preferred PHP Variables Dumper.
